<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSerie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->rol->id === 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $serie_id = $this->route()->parameter('series');

        return [
            'name' => [
                'required',
                'min:2',
                Rule::unique('series')->ignore($serie_id),
            ],
            'picture' => 'image',
            'information' => 'required|min:6',
            'trailer_url' => 'required',
            'tags' => 'required',
        ];
    }
}
