<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUser;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('name')->paginate();

        return view('users.index', compact('users'));
    }

    public function profile()
    {
        $user = auth()->user();
        $subscriptions = Subscription::where('user_id', $user->id)
            ->orderBy('ends_at', 'desc')
            ->paginate();

        return view('users.edit', compact('user', 'subscriptions'));
    }

    // Edita usuarios diferentes al logueado
    // Disponible sólo para el administrador
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $subscriptions = Subscription::where('user_id', $user->id)
            ->orderBy('ends_at', 'desc')
            ->paginate();

        return view('users.edit', compact('user', 'subscriptions'));
    }

    public function update(UpdateUser $request, $id)
    {
        $user = User::findOrFail($id);

        // Si el usuario está editando su perfil
        if ($user->id === auth()->user()->id) {
            $current_password = Hash::check($request->get('current_password'), $user->password);

            if (!$current_password) {
                return back()->with('info', 'La contraseña actual es incorrecta');
            }
        }

        if ($request->get('new_password')) {
            $user->password = bcrypt($request->get('new_password'));
        }

        if ($request->hasFile('picture')) {
            Storage::delete($user->picture);
            $user->picture = $request->file('picture')->store('public');
        }

        $user->save();

        if (auth()->user()->id === $request->route('user')) {
            // Reautentica al usuario
            auth()->login($user);
        }

        return back()->with('info', 'Perfil actualizado');
    }
}
