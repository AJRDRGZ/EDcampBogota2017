CREATE TABLE audits (
  id SERIAL NOT NULL,
  audit_action VARCHAR(30) NOT NULL,
  audit_table VARCHAR(50) NOT NULL,
  previous_data JSON NOT NULL,
  actual_data JSON,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP,
  CONSTRAINT audits_id_pk PRIMARY KEY (id)
);

COMMENT ON TABLE audits IS 'Auditoría de la información';
